//Mandeep Singh 1937332
package tests;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import utilities.MatrixMethod;
class MatrixMethodTests {

	@Test
	void testDuplicatedArray() {
		int[] [] originalArray = { {1,2,3},{4,5,6}};
		int [][] manuallyDuplicated = {{1,1,2,2,3,3},{4,4,5,5,6,6}};
		int[][] automaticallyDuplicated = MatrixMethod.duplicate(originalArray);
		assertArrayEquals(manuallyDuplicated,automaticallyDuplicated);
		
		}
	@Test
	void testDuplicatedArray2() {
		int[] [] originalArray = {{6,7,9,8},{5,2,0,1}};
		int [][] manuallyDuplicated = {{6,6,7,7,9,9,8,8},{5,5,2,2,0,0,1,1}};
		int[][] automaticallyDuplicated = MatrixMethod.duplicate(originalArray);
		assertArrayEquals(manuallyDuplicated,automaticallyDuplicated);
		
	}
	

}
