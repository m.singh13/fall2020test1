//Mandeep Singh 1937332
package tests;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import automobiles.Car;

class CarTests {
	
	//Creating a new Car with the speed of 30
	//Testing if the get methods give the expected values
	@Test
	void getTest() {
	Car testCar = new Car(30);
	assertEquals(30,testCar.getSpeed());
	assertEquals(50,testCar.getLocation());
		
	}
	//Creating a new Car with the speed of 10
	//Testing if the move methods give the expected values
	@Test
	void movingTest() {
		Car movingCar = new Car(100);
		movingCar.moveRight();
		assertEquals(100,movingCar.getLocation());
		movingCar.moveLeft();
		assertEquals(0,movingCar.getLocation());	
		
	}
	//Creating a new Car with the speed of 0
	//Testing if accelerate/stop methods give the expected values
	@Test
	void AccelerateStopTest(){
		Car regularCar = new Car(0);
		regularCar.accelerate();
		assertEquals(1,regularCar.getSpeed());
		regularCar.stop();
		assertEquals(0,regularCar.getSpeed());
		
	}
	//Creating a new Car with the speed of -50
	//Testing if it throws an IllegalArgumentException
	@Test 
	void testIllegalArgumentException() {
		try {
			Car illegalCar = new Car(-50);
		} catch (IllegalArgumentException ex) {
		assertEquals("Speed must not be negative!",ex.getMessage());	
	}


	}
}


