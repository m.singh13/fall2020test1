//Mandeep Singh 1937332
package utilities;

public class MatrixMethod {

public static int[][] duplicate(int[][] originalArray){
	
		//Initialize the duplicatedArray twice the size on the second index
		int [][] duplicatedArray = new int [originalArray.length][2*originalArray[0].length];
		//Start for loop that controls the first index
		for(int i=0;i<duplicatedArray.length;i++) {
			
			//Start for loop that controls the second index
			//Increment by 2, because 1 (same) element every 2 index
			for(int j=0;j<duplicatedArray[0].length;j+=2) {
				
			//Divide by 2 or else index out of bound
			duplicatedArray[i][j]=originalArray[i][j/2];
			duplicatedArray[i][j+1] = originalArray[i][j/2];
			}
		}
		
		return duplicatedArray;		
	}
}

